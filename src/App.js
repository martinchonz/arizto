import React, { Component } from 'react';
import './App.css';

import { Button } from 'react-bootstrap';
import ModalTask from './components/ModalTask';
import ModalNewTask from './components/ModalNewTask';

var FormGroup = require('react-bootstrap').FormGroup;
var FormControl = require('react-bootstrap').FormControl;

class App extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      smShow: false,
      fullShow: false,
      index: 0,
    };
    this.toggleClass = this.toggleClass.bind(this)
  }
  
  toggleClass(tabindex) {
    this.setState({ index:tabindex });
  }
  
  render() {
    let smClose = () => this.setState({ smShow: false });
    let fullClose = () => this.setState({ fullShow: false });
    let container = "container px-8";
    let seachIcon = "fas fa-search search-icon";
    let profileIcon = "far fa-user-circle text-3xl icon-weight";
    let addIcon = "fas fa-plus f-full flex items-center text-xl";
    let folderIcon = "far fa-folder-open h-full flex items-center text-xl";
  
    return (
    <div className="h-screen relative">
      <div className={ container }>
        {/* modal task */}
        <Button
          bsStyle="primary"
          onClick={() => this.setState({ smShow: true })}
        >
          modal1
        </Button>

        <ModalTask show={ this.state.smShow } onHide={ smClose } />

        {/* modal new task */}
        <Button
          bsStyle="primary"
          onClick={() => this.setState({ fullShow: true })}
        >
          modal2
        </Button>

        <ModalNewTask show={ this.state.fullShow } onHide={ fullClose } />
        
        <header className="App-header">
          <div className="header-top">
            <h1>Home</h1>
            <div className="top-profile">
              <span className="text-center"><i className={ profileIcon }></i></span>
              <span>My Profile</span>
            </div>
          </div>
          <div className="header-tab">
            <div className="tab" onClick={ this.toggleClass.bind(0) }><span>Task (10)</span></div>
            <div className="tab" onClick={ this.toggleClass.bind(1) }><span>Notifications (12)</span></div>
          </div>
          <div className="header-bot">
            <form className="w-3/4">
              <FormGroup className="relative mb-0">
                <FormControl type="text" placeholder="Search" />
                <i className={ seachIcon }></i>
              </FormGroup>
            </form>
            <div className="bot-buttons-wrap">
              <i className={ addIcon }></i>
              <i className={ folderIcon }></i>
            </div>
          </div>
        </header>
        <section>
          3.contents
            <p>task card</p>
        </section>
      </div>
      <div className="footer">
        <ul class="footer-list-wrap">
          <li class="footer-list-items">
            <span className="item-icon"><i class="far fa-check-square"></i></span>
            <span className="item-name">home</span>
          </li>
          <li class="footer-list-items">
            <span className="item-icon"><i class="fab fa-gripfire"></i></span>
            <span className="item-name">leads</span>
          </li>
          <li class="footer-list-items">
            <span className="item-icon"><i class="fas fa-sign"></i></span>
            <span className="item-name">listing</span>
          </li>
          <li class="footer-list-items">
            <span className="item-icon"><i class="far fa-calendar-alt"></i></span>
            <span className="item-name">calendar</span>
          </li>
          <li class="footer-list-items">
            <span className="item-icon"><i class="far fa-chart-bar"></i></span>
            <span className="item-name">insights</span>
          </li>
        </ul>
      </div>
    </div>
    );
  }
}

export default App;
