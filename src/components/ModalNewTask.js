import React, {Component} from 'react';
import { Button } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';


var Modal = require('react-bootstrap').Modal;
var FormGroup = require('react-bootstrap').FormGroup;
var ControlLabel = require('react-bootstrap').ControlLabel;
var FormControl = require('react-bootstrap').FormControl;
var FormGroup = require('react-bootstrap').FormGroup;
var HelpBlock = require('react-bootstrap').HelpBlock;


function FieldGroup({ id, label, help, ...props }) {
    
    return (
        <FormGroup controlId={id}>
        <ControlLabel>{label}</ControlLabel>
        <FormControl {...props} />
        {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    );
}

class ModalNewTask extends Component {

    constructor(props, context) {
        super(props, context);
        
        this.handleChange = this.handleChange.bind(this);
    
        this.state = {
            name: '',
            description: '',
            startDate: null
        };
        this.dateHandleChange = this.dateHandleChange.bind(this);
    }

    getInitialState() {
        var value = new Date().toISOString();
        return {
            value: value
        }
    }

    dateHandleChange(date) {
        this.setState({
            startDate: date
        });
    }

    getValidationState() {
        const name = this.state.name.length;
        const description = this.state.name.length;
        if (name > 70) return 'error';
        else if (description > 70 ) return 'error';
        return null;
    }
    
    handleChange(e) {
        this.setState(
            { name: e.target.value },
            { description: e.target.description }
        );
    }
    

    render() {

        return (
            <Modal
            {...this.props}
            bsSize="lg"
            aria-labelledby="contained-modal-title-lg"
            className="modal-new-task"
            >
                <Modal.Header closeButton>
                </Modal.Header>
                <Modal.Body>
                    <h4 className="modal-task-title">New Task</h4>
                    <p className="modal-task-p">Non, tempora quisquam eum quis in magni placeat ipsum doloremque alias eos?</p>
                    

                    <form>
                        <FormGroup
                        controlId="formBasicText"
                        validationState={this.getValidationState()}
                        >
                        <ControlLabel>Type of Task</ControlLabel>
                        <FormControl componentClass="select" placeholder="Choose Task Type" className="mb-8">
                            <option value="1">Select one</option>
                            <option value="2">Select two</option>
                            <option value="3">Select three</option>
                        </FormControl>
                    
                        <FieldGroup
                            id="taskName"
                            type="text"
                            label="Task Name"
                            placeholder="Enter a Task Description (max 70 characters)"
                        />
                        <FieldGroup
                            id="taskDescription"
                            type="text"
                            label="Task Description"
                            placeholder="Enter a Task Description (max 70 characters)"
                        />

                        <ControlLabel>Due Date</ControlLabel>
                        <DatePicker
                            selected={this.state.startDate}
                            onChange={this.dateHandleChange}
                            placeholderText="No specific Date"
                        />
                        </FormGroup>
                        
                        <Button type="submit" className="task-form-submit">Next</Button>
                    </form>

                </Modal.Body>
            </Modal>
        );
    }
}


export default ModalNewTask;